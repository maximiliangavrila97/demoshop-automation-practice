package org.fasttrackit;

public class DemoShop {
    public static void main(String[] args) {
        System.out.println("Demo Shop");
        System.out.println("1. User can login with valid credentials.");
        Page page = new Page();
        page.openHomePage();
        Header header = new Header();
        header.clickOnTheLoginButton();
        ModalDialog modal = new ModalDialog();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        System.out.println("Hi dino is displayed in the header" + isLogged);
        System.out.println("Greetings msg is: " + greetingsMessage);

        System.out.println("----------------------------------------");
        System.out.println("2.User can Add product to cart from product cards");
        page.openHomePage();
        ProductCards cards = new ProductCards();

        System.out.println("----------------------------------------");
        System.out.println("3. User can navigate to Home Page from WishList Page");
        page.openHomePage();
        header.clickOnTheWishListIcon();
        String pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Wishlist page. Title is:  " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Home page. Title is: " + pageTitle);

        System.out.println("------------------------------------------");
        System.out.println("4. User can navigate to Home Page from Cart Page.");
        page.openHomePage();
        header.clickOnTheCartIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Cart page page " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Home Page. " + pageTitle);
    }
}


// 1. User can add Awesome Soft Shirt to wishlist page
// - Access Home Page
// - Click on Awesome Soft Shirt product's wishlist heart
// - Expected: Expected to show Awesome Soft Shirt on the wishlist page.