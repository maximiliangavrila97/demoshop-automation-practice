package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.*;

public class Page {
    private final SelenideElement pageTitle = $(".subheader-container .text-muted");

    public static final String URL = "https://fasttrackit-test.netlify.app";


    @Step("Open Demo shop page.")
    public void openHomePage () {
        System.out.println("Opening: " + URL);
        open(URL);
    }
    public String getPageTitle () {
        return pageTitle.text();
    }
}
