package org.fasttrackit;


import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.*;


@Epic("Login")
@Feature("User can Login to DemoShop App.")
public class DemoShopTest {

    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up () {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test(description = "User turtle can login with valid credentials.")
    @Description ("User Turtle can login with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Gavrila Maximilian")
    @Lead("Ion Rosu")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Issue("DMS-001")
    @Story("Login with valid credentials")
     public void user_turtle_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi turtle!", "Logged in with turtle, expected greetings message to be Hi turtle!");
    }
    @Test
    public void user_dino_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi dino!", "Logged in with dino, expected greetings message to be Hi dino!");
    }

    @Test
    public void user_can_navigate_to_WishList_Page() {
        header.clickOnTheWishListIcon();
        assertEquals(page.getPageTitle(),"Wishlist", "Expected to be on the Wishlist page!");
    }
    @Test
    public void user_can_navigate_to_cart_page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(),"Your cart","Expected to be on the Cart page");
    }

    @Test
    public void user_can_navigate_to_Home_Page_from_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(),"Products", "Expected to be on the Products page");
    }

    @Test(dependsOnMethods = "user_can_navigate_to_cart_page")
    public void user_can_navigate_to_Home_Page_from_Cart_Page() {
        header.clickOnTheShoppingBagIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(),"Products", "Expected to be on the Products page");
    }

    @Test
    public void user_can_add_product_to_cart_from_product_cards(){
        ProductCards cards = new ProductCards();
        Product awesomeGraniteChips = new Product("9");
        awesomeGraniteChips.clickOnTheProductCartIcon();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1. ");
        System.out.println("Select product: " + awesomeGraniteChips.getTitle());
    }
    @Test
    public void user_can_add_two_products_to_cart_from_product_cards(){
        ProductCards cards = new ProductCards();
        Product awesomeGraniteChips = new Product("9");
        awesomeGraniteChips.clickOnTheProductCartIcon();
        awesomeGraniteChips.clickOnTheProductCartIcon();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two products to cart, badge shows 2. ");
        System.out.println("Select product: " + awesomeGraniteChips.getTitle());
    }
}