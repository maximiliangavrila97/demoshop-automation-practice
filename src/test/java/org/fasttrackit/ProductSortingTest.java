package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Ignore
public class ProductSortingTest  {
    Page page = new Page();
    ProductCards productList = new ProductCards();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup () {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }


    @Test
    public void when_sorting_products_A_to_Z_products_are_sorted_alphabetically() {
        Product firstProductBeforeSort = productList.getFirstproductInList();
        Product lastproductBeforeSort = productList.getLastproductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheAZSortButton();
        Product firstProductAfterSort = productList.getFirstproductInList();
        Product lastproductAfterSort = productList.getLastproductInList();

        assertEquals(firstProductAfterSort.getTitle(),firstProductBeforeSort.getTitle());
        assertEquals(lastproductAfterSort.getTitle(),lastproductBeforeSort.getTitle());


    }
    @Test
    public void when_sorting_products_Z_to_A_products_are_sorted_alphabetically_DESC() {
        Product firstProductBeforeSort = productList.getFirstproductInList();
        Product lastproductBeforeSort = productList.getLastproductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheZASortButton();
        Product firstProductAfterSort = productList.getFirstproductInList();
        Product lastproductAfterSort = productList.getLastproductInList();

        assertEquals(firstProductAfterSort.getTitle(),lastproductBeforeSort.getTitle());
        assertEquals(lastproductAfterSort.getTitle(),firstProductBeforeSort.getTitle());


    }
    @Test
    public void when_sorting_products_by_price_low_to_high_products_are_sorted_by_price_low_to_high() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstproductInList();
        Product lastproductBeforeSort = productList.getLastproductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductAfterSort = productList.getFirstproductInList();
        Product lastproductAfterSort = productList.getLastproductInList();

        assertEquals(firstProductAfterSort.getPrice(),firstProductBeforeSort.getPrice());
        assertEquals(lastproductAfterSort.getPrice(),lastproductBeforeSort.getPrice());


    }
    @Test
    public void when_sorting_products_by_price_high_to_low_products_are_sorted_by_price_high_to_low() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstproductInList();
        Product lastproductBeforeSort = productList.getLastproductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceHilo();
        Product firstProductAfterSort = productList.getFirstproductInList();
        Product lastproductAfterSort = productList.getLastproductInList();

        assertEquals(firstProductAfterSort.getPrice(),lastproductBeforeSort.getPrice());
        assertEquals(lastproductAfterSort.getPrice(),firstProductBeforeSort.getPrice());


    }
}

